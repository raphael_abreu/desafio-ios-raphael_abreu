//
//  PullTableViewController.swift
//  DesafioConcreteIOS
//
//  Created by RAPHAEL P A SANTOS on 10/06/16.
//  Copyright © 2016 RAPHAEL P A SANTOS. All rights reserved.
//

import UIKit

class PullTableViewController: UITableViewController {
    
    var repository: Repository!
    var pullRequests = Array<PullRequest>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.clearsSelectionOnViewWillAppear = false
        self.navigationItem.title = repository.name

    }
    
    override func viewWillAppear(animated: Bool) {
        
        pullRequests = RepositoryDAO.getPRRepository(repository.fullName!)
        super.viewWillAppear(true)
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return pullRequests.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("customCell") as UITableViewCell!
        let pullRequest = pullRequests[indexPath.row]
        
        let title = cell.viewWithTag(1) as! UILabel
        title.text = pullRequest.title
        
        let body = cell.viewWithTag(2) as! UILabel
        body.text = pullRequest.body
        
        let imageView = cell.viewWithTag(3) as! UIImageView
        imageView.downloadedFrom(link: (pullRequest.owner?.avatarUrl)!, contentMode: .ScaleAspectFit)
        
        let userName = cell.viewWithTag(4) as! UILabel
        userName.text = pullRequest.owner?.nome
        
        let data = cell.viewWithTag(5) as! UILabel
        data.text = pullRequest.dataCreated
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let pullRequest = pullRequests[indexPath.row]
        
        UIApplication.sharedApplication().openURL(NSURL(string: pullRequest.htmlUrl!)!)
    }

}
