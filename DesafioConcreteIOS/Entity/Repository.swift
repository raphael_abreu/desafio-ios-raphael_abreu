//
//  Repository.swift
//  DesafioConcreteIOS
//
//  Created by RAPHAEL P A SANTOS on 10/06/16.
//  Copyright © 2016 RAPHAEL P A SANTOS. All rights reserved.
//

import Foundation
import ObjectMapper

class Repository: Mappable {
    
    var name: String?
    var fullName: String?
    var description: String?
    var stargazers: Int?
    var forks: Int?
    var owner: Owner?
    
    required init?(_ map: Map) {
        mapping(map)
    }
    
    // Mappable
    func mapping(map: Map) {
        name        <- map["name"]
        fullName    <- map["full_name"]
        description <- map["description"]
        stargazers  <- map["stargazers_count"]
        forks       <- map["forks"]
        owner       <- map["owner"]
    }
}
