//
//  Owner.swift
//  DesafioConcreteIOS
//
//  Created by RAPHAEL P A SANTOS on 10/06/16.
//  Copyright © 2016 RAPHAEL P A SANTOS. All rights reserved.
//

import Foundation
import ObjectMapper

class Owner: Mappable {
    
    var nome: String?
    var avatarUrl: String?
    
    required init?(_ map: Map) {
        mapping(map)
    }
    
    // Mappable
    func mapping(map: Map) {
        nome        <- map["login"]
        avatarUrl   <- map["avatar_url"]
    }
}