//
//  PullRequest.swift
//  DesafioConcreteIOS
//
//  Created by RAPHAEL P A SANTOS on 11/06/16.
//  Copyright © 2016 RAPHAEL P A SANTOS. All rights reserved.
//

import Foundation
import ObjectMapper

class PullRequest: Mappable {
    
    var title: String?
    var dataCreated: String?
    var body: String?
    var owner: Owner?
    var htmlUrl: String?
    
    required init?(_ map: Map) {
        mapping(map)
    }
    
    // Mappable
    func mapping(map: Map) {
        title        <- map["title"]
        dataCreated  <- map["created_at"]
        body         <- map["body"]
        owner        <- map["user"]
        htmlUrl      <- map["html_url"]
    }
}