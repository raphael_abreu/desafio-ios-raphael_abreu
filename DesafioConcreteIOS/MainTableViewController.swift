//
//  MainTableViewController.swift
//  DesafioConcreteIOS
//
//  Created by RAPHAEL P A SANTOS on 07/06/16.
//  Copyright © 2016 RAPHAEL P A SANTOS. All rights reserved.
//

import UIKit


class MainTableViewController: UITableViewController {
    
    var repositories = Array<Repository>()
    var page = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        
        repositories = RepositoryDAO.getRepositories(page)
        
        tableView.infiniteScrollIndicatorStyle = .White
        tableView.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: CGRectMake(0, 0, 24, 24))
        tableView.infiniteScrollIndicatorMargin = 40
        tableView.addInfiniteScrollWithHandler { (scrollView) -> Void in
            let tableView = scrollView as! UITableView
            
            self.page += 1
            self.repositories += RepositoryDAO.getRepositories(self.page)
            
            tableView.reloadData()
            tableView.finishInfiniteScroll()
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "PullRequest" {
            
            if let indexPath = self.tableView.indexPathForSelectedRow {
                
                let object = repositories[indexPath.row]
                let vc = segue.destinationViewController as! PullTableViewController
                vc.repository = object
            }
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("customCell") as UITableViewCell!
        let repository = repositories[indexPath.row]
        
        let nameRepository = cell.viewWithTag(1) as! UILabel
        nameRepository.text = repository.name
        
        let descRepository = cell.viewWithTag(2) as! UILabel
        descRepository.text = repository.description
        
        let fork = cell.viewWithTag(3) as! UILabel
        fork.text = String(repository.forks!)
        
        let star = cell.viewWithTag(4) as! UILabel
        star.text = String(repository.stargazers!)
        
        let imageView = cell.viewWithTag(5) as! UIImageView
        imageView.downloadedFrom(link: (repository.owner?.avatarUrl)!, contentMode: .ScaleAspectFit)
        
        let userName = cell.viewWithTag(6) as! UILabel
        userName.text = repository.owner?.nome
        
        let fullName = cell.viewWithTag(7) as! UILabel
        fullName.text = repository.fullName

        return cell
    }
}

