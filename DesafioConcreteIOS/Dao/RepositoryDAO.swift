//
//  RepositoryDAO.swift
//  DesafioConcreteIOS
//
//  Created by RAPHAEL P A SANTOS on 10/06/16.
//  Copyright © 2016 RAPHAEL P A SANTOS. All rights reserved.
//

import Foundation
import ObjectMapper

class RepositoryDAO {
    
    class func getRepositories(page: Int) -> [Repository] {
        
        var repositories : Array<Repository> = Array<Repository>()
        let manager = AFHTTPRequestOperationManager()
        var waiting = true
        
        manager.GET("https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(page)", parameters: nil,
            success: { operation, responseObject in
                
                if let items = responseObject.objectForKey("items") as? [NSDictionary] {
                    
                    for item in items {
                        if let repository = Mapper<Repository>().map(item) {
                            repositories.append(repository)
                        }
                    }
                    
                } else {
                    print("no items")
                }
                waiting = false
            },
            failure: { operation, error in
                
                print("Error: " + error.localizedDescription)
                waiting = false
            }
        )
        
        while(waiting) {
            NSRunLoop.currentRunLoop().runMode(NSDefaultRunLoopMode, beforeDate: NSDate())
            usleep(10)
        }
        
        return repositories
    }
    
    class func getPRRepository(fullname: String) -> [PullRequest] {
        
        var pullRequests : Array<PullRequest> = Array<PullRequest>()
        let manager = AFHTTPRequestOperationManager()
        var waiting = true
        
        manager.GET("https://api.github.com/repos/\(fullname)/pulls", parameters: nil,
                    success: { operation, responseObject in
                        
                        if let items = responseObject as? [NSDictionary] {
                            
                            for item in items {
                                if let pullRequest = Mapper<PullRequest>().map(item) {
                                    pullRequests.append(pullRequest)
                                }
                            }
                            
                        } else {
                            print("no items")
                        }
                        waiting = false
            },
                    failure: { operation, error in
                        
                        print("Error: " + error.localizedDescription)
                        waiting = false
            }
        )
        
        while(waiting) {
            NSRunLoop.currentRunLoop().runMode(NSDefaultRunLoopMode, beforeDate: NSDate())
            usleep(10)
        }
        
        return pullRequests
    }
}
